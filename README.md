# BNDL

A SPWN module bundler. Ironically written in JavaScript, might do a Rust rewrite if I have the time to + if it is possible.

# Usage
```
bndl bundle file.spwn -o output.spwn
```
file.spwn:
```
let module = import "some-module.spwn";
$.print(module); // Hello, World!
```
Output:
```
let module = (() {
  return "Hello, World!"
})();
$.print(module); // Hello, World!
```
