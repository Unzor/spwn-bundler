const fs = require("fs");


function bundle(t) {
    t = t.split(/([^\s,;]+)/g);
    var r = "";
    let ost = "";
    let nString = "";
    nString = t.join('');
    ost = nString;
    t.forEach((e, a) => {
      var cdir = __dirname + '/';
      var path = "";
        var u = !1; // u = enclosed
        '"' != e && "'" != e || "\\" === t[a - 1] || (u = !u), o = "\n" !== e
        if (!u && e == "import") {
            var mod = t[a + 2];
            if (mod.startsWith("\"") && mod.endsWith("\"")) {
                path = mod.slice(1, -1);
                ost = ost.replace(t[a] + ' ' + t[a + 2], `(() {
  ${fs.readFileSync(path).toString()}
})()`);
            } else {
                path = "libraries/" + mod + "/lib.spwn";
                ost = ost.replace(t[a] + ' ' + t[a + 2], `(() {
  ${fs.readFileSync(path).toString()}
})()`);
            }

        }
    });
    return ost;
}
module.exports = bundle;
